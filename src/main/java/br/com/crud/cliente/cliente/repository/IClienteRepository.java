package br.com.crud.cliente.cliente.repository;

import br.com.crud.cliente.cliente.model.Cliente;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.concurrent.CompletableFuture;

public interface IClienteRepository extends JpaRepository<Cliente, Long> {

    CompletableFuture<Void> deleteById(Long clienteId);

}
