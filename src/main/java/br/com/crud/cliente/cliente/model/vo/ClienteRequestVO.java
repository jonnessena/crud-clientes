package br.com.crud.cliente.cliente.model.vo;

import br.com.crud.cliente.cidade.model.Cidade;
import br.com.crud.cliente.common.domain.SexoCliente;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.util.Date;

@Getter
@Setter
public class ClienteRequestVO {

    private String nomeCompleto;

    private SexoCliente sexo;

    @Temporal(TemporalType.TIMESTAMP)
    private Date dataNascimento;

    private Integer idade;

    private Cidade cidade;
}
