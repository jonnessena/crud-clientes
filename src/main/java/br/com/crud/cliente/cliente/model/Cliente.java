package br.com.crud.cliente.cliente.model;

import br.com.crud.cliente.cidade.model.Cidade;
import br.com.crud.cliente.common.Auditoria;
import br.com.crud.cliente.common.domain.SexoCliente;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table
public class Cliente extends Auditoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column
    private String nomeCompleto;

    @Enumerated(EnumType.ORDINAL)
    @Column
    private SexoCliente sexo;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(nullable = false, updatable = false)
    private Date dataNascimento;

    @Column
    private Integer idade;

    //Foi colocado EAGER para que não dar problema de proxy
    //Como a entidade de retorno é pequena, foi optada essa estratégia
    @ManyToOne(fetch = FetchType.EAGER, optional = false)
    @JoinColumn(name = "cidade_id", nullable = false)
    private Cidade cidade;
}
