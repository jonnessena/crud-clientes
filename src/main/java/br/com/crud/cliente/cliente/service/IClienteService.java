package br.com.crud.cliente.cliente.service;

import br.com.crud.cliente.cliente.model.Cliente;
import br.com.crud.cliente.cliente.model.vo.ClienteFilterVO;
import br.com.crud.cliente.cliente.model.vo.ClienteRequestVO;
import br.com.crud.cliente.common.error.CrudClienteBusinessException;
import org.springframework.scheduling.annotation.Async;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface IClienteService {

    CompletableFuture<List<Cliente>> findAll(ClienteFilterVO example);

    @Async
    CompletableFuture<Cliente> saveAndFlush(ClienteRequestVO clienteRequestVO);

    @Async
    CompletableFuture<Cliente> saveAndFlush(Cliente cliente) throws Exception;

    CompletableFuture<String> deleteById(Long clienteId) throws CrudClienteBusinessException;
}
