package br.com.crud.cliente.cliente.model.vo;

import br.com.crud.cliente.common.domain.SexoCliente;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ClienteFilterVO {

    private String nomeCompleto;

    private String id;

    SexoCliente sexo;

    Integer idade;
}
