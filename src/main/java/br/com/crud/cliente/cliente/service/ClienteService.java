package br.com.crud.cliente.cliente.service;

import br.com.crud.cliente.cidade.model.Cidade;
import br.com.crud.cliente.cidade.service.ICidadeService;
import br.com.crud.cliente.cliente.model.Cliente;
import br.com.crud.cliente.cliente.model.vo.ClienteFilterVO;
import br.com.crud.cliente.cliente.model.vo.ClienteRequestVO;
import br.com.crud.cliente.cliente.repository.IClienteRepository;
import br.com.crud.cliente.common.error.CrudClienteBusinessException;
import ma.glasnost.orika.MapperFacade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class ClienteService implements IClienteService {

    @Autowired
    IClienteRepository clienteRepository;

    @Autowired
    ICidadeService cidadeRepository;

    @Autowired
    MapperFacade mapperFacade;

    @Override
    public CompletableFuture<List<Cliente>> findAll(ClienteFilterVO vo) {
        Cliente cliente = mapperFacade.map(vo, Cliente.class);

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("nomeCompleto", ExampleMatcher.GenericPropertyMatchers.ignoreCase())
                .withMatcher("idade", ExampleMatcher.GenericPropertyMatchers.ignoreCase())
                .withMatcher("sexo", ExampleMatcher.GenericPropertyMatchers.ignoreCase())
                .withMatcher("id", ExampleMatcher.GenericPropertyMatchers.ignoreCase());

        Example<Cliente> clienteExample = Example.of(cliente, matcher);
        return CompletableFuture.completedFuture(clienteRepository.findAll(clienteExample));
    }

    @Override
    public CompletableFuture<Cliente> saveAndFlush(ClienteRequestVO vo) {
        Cliente cliente = mapperFacade.map(vo, Cliente.class);

        return CompletableFuture.completedFuture(clienteRepository.saveAndFlush(cliente));
    }

    @Override
    public CompletableFuture<String> deleteById(Long clienteId) throws CrudClienteBusinessException {
        Cliente cliente = new Cliente();
        cliente.setId(clienteId);

        Cliente econtrado = clienteRepository.findOne(Example.of(cliente));

        if (econtrado == null) {
            throw new CrudClienteBusinessException(HttpStatus.BAD_REQUEST, "Não foi possível realizar a exclusão. " +
                    "Cliente não existe");
        }

        clienteRepository.delete(clienteId);
        return CompletableFuture.completedFuture("Operação realizada com sucesso.");
    }

    public CompletableFuture<Cliente> saveAndFlush(Cliente cliente) throws Exception {

        if (cliente.getId() != null &&
                cliente.getCidade() != null && cliente.getCidade().getId() != null) {

            Cidade cidadeRetornada = cidadeRepository.findOne(cliente.getCidade().getId()).get();

            if (cidadeRetornada == null) {
                throw new CrudClienteBusinessException(HttpStatus.BAD_REQUEST, "Cidade não cadastrada.");
            }

            cliente.setCidade(cidadeRetornada);
        } else {
            throw new CrudClienteBusinessException(HttpStatus.BAD_REQUEST, "Cidade não informada.");
        }
        return CompletableFuture.completedFuture(clienteRepository.saveAndFlush(cliente));
    }


}
