package br.com.crud.cliente.cliente.controller;

import br.com.crud.cliente.cliente.model.Cliente;
import br.com.crud.cliente.cliente.model.vo.ClienteFilterVO;
import br.com.crud.cliente.cliente.model.vo.ClienteRequestVO;
import br.com.crud.cliente.cliente.service.IClienteService;
import br.com.crud.cliente.common.error.CrudClienteBusinessException;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("cliente")
public class ClienteController {

    @Autowired
    IClienteService clienteService;

    @GetMapping
    public CompletableFuture<List<Cliente>> findClientes(@ApiParam @Valid ClienteFilterVO requestVO) {
        return clienteService.findAll(requestVO);
    }

    @PostMapping
    public CompletableFuture<Cliente> save(@RequestBody @Valid ClienteRequestVO requestVO) {
        return clienteService.saveAndFlush(requestVO);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public CompletableFuture<String> delete(@PathVariable(value = "id") String clienteID)
            throws CrudClienteBusinessException {
        return clienteService.deleteById(Long.valueOf(clienteID));
    }

    @PutMapping
    @ResponseBody
    public CompletableFuture<Cliente> update(@RequestBody Cliente cliente) throws Exception {
        return clienteService.saveAndFlush(cliente);
    }
}
