package br.com.crud.cliente.cidade.service;

import br.com.crud.cliente.cidade.model.Cidade;

import java.util.List;
import java.util.concurrent.CompletableFuture;

public interface ICidadeService {

    CompletableFuture<List<Cidade>> findByCidadeNomeOrEstado(String nome, String estado);

    CompletableFuture<Cidade> save(Cidade entity);

    CompletableFuture<Cidade> findOne(Long id);

}
