package br.com.crud.cliente.cidade.service;

import br.com.crud.cliente.cidade.model.Cidade;
import br.com.crud.cliente.cidade.repository.ICidadeRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.CompletableFuture;

@Service
public class CidadeService implements ICidadeService {

    @Autowired
    ICidadeRepository cidadeRepository;

    @Override
    public CompletableFuture<List<Cidade>> findByCidadeNomeOrEstado(String nome, String estado) {

        ExampleMatcher matcher = ExampleMatcher.matching()
                .withMatcher("nome", ExampleMatcher.GenericPropertyMatchers.ignoreCase())
                .withMatcher("estado", ExampleMatcher.GenericPropertyMatchers.ignoreCase());

        Example<Cidade> cidadeExample = Example.of(new Cidade(nome, estado), matcher);

        return CompletableFuture.completedFuture(cidadeRepository.findAll(cidadeExample));
    }

    @Override
    public CompletableFuture<Cidade> save(Cidade cidade) {
        return CompletableFuture.completedFuture(cidadeRepository.save(cidade));
    }

    @Override
    public CompletableFuture<Cidade> findOne(Long id) {
        return CompletableFuture.completedFuture(cidadeRepository.findOne(id));
    }
}
