package br.com.crud.cliente.cidade.model;

import br.com.crud.cliente.common.Auditoria;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Cidade extends Auditoria {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @NotNull
    @Column(nullable = false)
    String nome;

    @NotNull
    @Column(nullable = false)
    String estado;

    public Cidade(String nome, String estado) {
        this.nome = nome;
        this.estado = estado;
    }
}
