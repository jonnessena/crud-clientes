package br.com.crud.cliente.cidade.controller;

import br.com.crud.cliente.cidade.model.Cidade;
import br.com.crud.cliente.cidade.model.vo.CidadeRequestVO;
import br.com.crud.cliente.cidade.service.ICidadeService;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.concurrent.CompletableFuture;

@RestController
@RequestMapping("cidade")
public class CidadeController {

    @Autowired
    ICidadeService cidadeService;

    @GetMapping
    public CompletableFuture<List<Cidade>> cidadesByExample(
            @ApiParam(value = "Filtros de cidades", required = true) @Valid CidadeRequestVO request) {

        return cidadeService.findByCidadeNomeOrEstado(request.getNome(), request.getEstado());
    }

    @PostMapping
    public CompletableFuture<Cidade> save(@RequestBody @Valid Cidade cidade) {
        return cidadeService.save(cidade);
    }

}
