package br.com.crud.cliente.cidade.model.vo;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CidadeRequestVO {
    String estado;
    String nome;
}
