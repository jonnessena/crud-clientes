package br.com.crud.cliente.common.error;

import org.springframework.http.HttpStatus;

import java.util.List;

public class CrudClienteInfraException extends RuntimeException {
    private static final long serialVersionUID = -4000831586710267071L;

    private HttpStatus code;
    private String msgCode;
    private List<String> arguments;

    public CrudClienteInfraException(Throwable t) {
        super(t);
        this.code = HttpStatus.INTERNAL_SERVER_ERROR;
        this.msgCode = "global.message.internalServerError";

    }
}
