package br.com.crud.cliente.common.error;


import lombok.extern.slf4j.Slf4j;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

@ControllerAdvice
@Slf4j
public class GlobalExceptionController {

    @ExceptionHandler(RuntimeException.class)
    @ResponseBody
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    public Object handleAllException(RuntimeException ex) {

        if (ex.getCause() != null) {
            Throwable cause = ex.getCause();
            do {
                if (cause instanceof CrudClienteBusinessException)
                    return handleBusinessException((CrudClienteBusinessException) cause);
                cause = cause.getCause();
            } while (cause != null);

        }

        String uuid = UUID.randomUUID().toString();
        log.error(uuid, ex);

        List<String> mensagens = new ArrayList<>();
        mensagens.add(ex.getMessage());

        return new CrudClienteBusinessException(HttpStatus.INTERNAL_SERVER_ERROR, "Erro interno no servidor", mensagens);

    }

    @ExceptionHandler(CrudClienteBusinessException.class)
    @ResponseBody
    public ResponseEntity handleBusinessException(CrudClienteBusinessException ex) {

        return new ResponseEntity(ex, HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @ExceptionHandler({MethodArgumentNotValidException.class, DataIntegrityViolationException.class})
    @ResponseBody
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public CrudClienteBusinessException handleMethodArgumentException(MethodArgumentNotValidException ex) {

        List<String> fieldMapper = new ArrayList<>();
        for (FieldError fieldError : ex.getBindingResult().getFieldErrors()) {
            fieldMapper.add(fieldError.getField());
        }

        return new CrudClienteBusinessException(HttpStatus.BAD_REQUEST, "Erro interno no servidor", fieldMapper);

    }
}