package br.com.crud.cliente.common.cruds;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.io.Serializable;
import java.util.List;
import java.util.concurrent.CompletableFuture;

public abstract class AsyncCrudService<T, ID extends Serializable> implements IAsyncCrudService<T, ID> {

    @Autowired
    private AsyncRepository<T, ID> asyncRepository;

    @Override
    public long count() {
        return asyncRepository.count();
    }

    @Override
    public void delete(ID id) {
        asyncRepository.delete(id);
        //return messageSource.getMessage("sucesso",null, Locale.getDefault());
    }

    @Override
    public String delete(Iterable<? extends T> entities) {
        asyncRepository.delete(entities);
        return "Operação efetuada com sucesso.";
    }

    @Override
    public String delete(T entity) {
        asyncRepository.delete(entity);
        return "Operação efetuada com sucesso.";
    }

    @Override
    public void deleteAll() {
        asyncRepository.deleteAll();
    }

    @Override
    public boolean exists(ID id) {
        return asyncRepository.exists(id);
    }

    @Override
    public CompletableFuture<List<T>> findBySonho() {
        return asyncRepository.findAll();
    }

    @Override
    public CompletableFuture<List<T>> findAll(Iterable<ID> ids) {
        return asyncRepository.findAll(ids);
    }

    @Override
    public CompletableFuture<T> findOne(ID id) {
        return asyncRepository.findOne(id);
    }

    @Override
    public CompletableFuture<Iterable<T>> save(Iterable<T> entities) {
        return asyncRepository.save(entities);
    }

    @Override
    public CompletableFuture<T> save(T entity) {
        return asyncRepository.save(entity);
    }

    @Override
    public CompletableFuture<Iterable<T>> findAll(Sort sort) {
        return asyncRepository.findAll(sort);
    }

    @Override
    public CompletableFuture<Page<T>> findAll(Pageable pageable) {
        return asyncRepository.findAll(pageable);
    }

    @Override
    public CompletableFuture<List<T>> findAll() {
        return asyncRepository.findAll();
    }
}
