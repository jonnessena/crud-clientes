package br.com.crud.cliente.common.async;

import org.springframework.core.task.SimpleAsyncTaskExecutor;
import org.springframework.web.context.request.RequestContextHolder;

/**
 * Created by gaetani on 29/11/17.
 */
public class ContextAwarePoolExecutor extends SimpleAsyncTaskExecutor {

    private ContextAwarePoolExecutor() {
        super("CrudClienteThread-");
    }

    private static ContextAwarePoolExecutor INSTANCE = new ContextAwarePoolExecutor();

    public static ContextAwarePoolExecutor getInstance() {
        return INSTANCE;
    }

    @Override
    public void execute(Runnable task) {
        super.execute(new ContextAwareTask(task, RequestContextHolder.currentRequestAttributes()));
    }

    @Override
    public void execute(Runnable task, long startTimeout) {
        super.execute(new ContextAwareTask(task, RequestContextHolder.currentRequestAttributes()), startTimeout);
    }
}
