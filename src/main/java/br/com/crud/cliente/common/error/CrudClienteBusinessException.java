package br.com.crud.cliente.common.error;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.http.HttpStatus;

import java.util.List;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CrudClienteBusinessException extends Exception {

    private static final long serialVersionUID = -1493218627720771902L;

    private HttpStatus code;
    private String msgCode;
    private List<String> arguments;

    public CrudClienteBusinessException(HttpStatus code, String msgCode) {
        this.code = code;
        this.msgCode = msgCode;
    }

    public CrudClienteBusinessException(String msgCode) {
        this.msgCode = msgCode;
    }
}
