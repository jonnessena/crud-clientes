package br.com.crud.cliente.common.cruds;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.io.Serializable;
import java.util.concurrent.CompletableFuture;

public abstract class CrudController<T, ID extends Serializable> {

    @Autowired
    private IAsyncCrudService<T, ID> asyncCrudService;

    @GetMapping
    public CompletableFuture<Page<T>> findAll(Pageable pageable) {
        return asyncCrudService.findAll(pageable);
    }

    @GetMapping("/{id}")
    public CompletableFuture<T> findOne(@PathVariable("id") ID id) {
        return asyncCrudService.findOne(id);
    }

    @PostMapping
    public CompletableFuture<T> save(@RequestBody @Valid T entity) {
        return asyncCrudService.save(entity);
    }

    @PutMapping("/{id}")
    public CompletableFuture<T> update(@PathVariable("id") ID id, @RequestBody @Valid T entity) {
        return asyncCrudService.save(entity);
    }

    @DeleteMapping("/{id}")
    @ResponseBody
    public ResponseEntity<String> delete(@PathVariable("id") ID id) {
        asyncCrudService.delete(id);
        return buildOkMessage();
    }

    protected ResponseEntity<String> buildOkMessage() {
        return ResponseEntity.ok("Operação realizada com sucesso.");
    }
}
