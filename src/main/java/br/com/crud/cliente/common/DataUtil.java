package br.com.crud.cliente.common;

import org.apache.commons.lang3.time.DateUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Calendar;
import java.util.Date;

public class DataUtil {

    public static Integer diferencaEmDias(Date data1, Date data2) {
        return DataUtil.diferencaEmDias(DataUtil.toLocalDateTime(data1), DataUtil.toLocalDateTime(data2));
    }

    public static Integer diferencaEmMeses(Date data1, Date data2) {
        return DataUtil.diferencaEmMeses(DataUtil.toLocalDateTime(data1), DataUtil.toLocalDateTime(data2));
    }

    public static Integer diferencaEmSemanas(Date data1, Date data2) {
        return DataUtil.diferencaEmSemanas(DataUtil.toLocalDateTime(data1), DataUtil.toLocalDateTime(data2));
    }

    public static Integer diferencaEmDias(LocalDateTime localDateTime1, LocalDateTime localDateTime2) {
        return DataUtil.diferencaEm(ChronoUnit.DAYS, localDateTime1, localDateTime2);
    }

    public static Integer diferencaEmSemanas(LocalDateTime localDateTime1, LocalDateTime localDateTime2) {
        return DataUtil.diferencaEm(ChronoUnit.WEEKS, localDateTime1, localDateTime2);
    }

    public static Integer diferencaEmMeses(LocalDateTime localDateTime1, LocalDateTime localDateTime2) {
        return DataUtil.diferencaEm(ChronoUnit.MONTHS, localDateTime1, localDateTime2);
    }

    private static Integer diferencaEm(ChronoUnit unidade, LocalDateTime localDateTime1, LocalDateTime localDateTime2) {
        return Math.abs(Math.toIntExact(unidade.between(localDateTime1, localDateTime2)));
    }

    public static LocalDateTime toLocalDateTime(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
    }

    public static LocalTime toLocalTime(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalTime();
    }

    public static LocalDate toLocalDate(Date date) {
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    public static Date fromLocalDateToDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date fromLocalDateTimeToDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.toLocalDate().atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

    public static Date firstHourOfDay(Date date) {
        Date dateReturn = DateUtils.setHours(date, 0);
        dateReturn = DateUtils.setMinutes(dateReturn, 0);
        dateReturn = DateUtils.setSeconds(dateReturn, 0);
        dateReturn = DateUtils.setMilliseconds(dateReturn, 0);

        return dateReturn;
    }

    public static Date lastHourOfDay(Date date) {
        Date dateReturn = DateUtils.setHours(date, 23);
        dateReturn = DateUtils.setMinutes(dateReturn, 59);
        dateReturn = DateUtils.setSeconds(dateReturn, 59);
        dateReturn = DateUtils.setMilliseconds(dateReturn, 9);

        return dateReturn;
    }

    public static Date yesterday() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    public static Date lastMonth() {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -30);
        return cal.getTime();
    }

    public static Date qtdMonth(Integer meses) {
        final Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -(30 * meses));
        return cal.getTime();
    }

    public static Date today() {
        return new Date();
    }
}
