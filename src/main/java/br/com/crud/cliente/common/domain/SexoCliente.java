package br.com.crud.cliente.common.domain;

import lombok.Getter;

@Getter
public enum SexoCliente {
    M,
    F
}
