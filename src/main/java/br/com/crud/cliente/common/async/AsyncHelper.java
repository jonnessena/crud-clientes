package br.com.crud.cliente.common.async;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.CompletableFuture;

import static java.util.stream.Collectors.toList;

public class AsyncHelper {

    public static CompletableFuture<List<Object>> myAllOf(CompletableFuture<?>... futures) {
        return CompletableFuture.allOf(futures)
                .thenApply(x -> Arrays.stream(futures)
                        .map(f -> (Object) f.join())
                        .collect(toList())
                );
    }
}
