package br.com.crud.cliente.common.cruds;

import org.springframework.data.domain.Example;

import java.util.concurrent.CompletableFuture;

public interface QueryByExampleExecutor<T> {

    <S extends T> CompletableFuture<S> findOne(Example<S> example);

    <S extends T> CompletableFuture<Iterable<S>> findAll(Example<S> example);

}
