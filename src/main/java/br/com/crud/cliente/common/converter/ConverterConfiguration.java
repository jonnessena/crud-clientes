package br.com.crud.cliente.common.converter;

import br.com.crud.cliente.cliente.model.Cliente;
import br.com.crud.cliente.cliente.model.vo.ClienteRequestVO;
import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.MapperFactory;
import ma.glasnost.orika.impl.DefaultMapperFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class ConverterConfiguration {

    @Bean
    public MapperFacade mapper() {
        MapperFactory factory = new DefaultMapperFactory.Builder().build();

        factory.classMap(ClienteRequestVO.class, Cliente.class)
                //.fieldMap("cidadeId", "cidade.id").add()
                .byDefault().register();

        return factory.getMapperFacade();
    }
}
